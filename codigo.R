#_______Projeto Fantasma________#
#_______Heitor Roncato__________#
library(tidyverse)
library(dplyr)
library(readxl)
library(stringr)
# Definindo paleta de cores da Estat
cores_estat <- c(
  "#A11D21", "#003366", "#CC9900", "#663333", "#FF6600",
  "#CC9966", "#999966", "#006606", "#008091", "#041835",
  "#666666"
)
options(
  ggplot2.discrete.color = cores_estat,
  ggplot2.discrete.fill = cores_estat
)



# Definindo tema de gr?ficos da Estat
theme_estat <- function(...) {
  ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    ) %>%
    return()
}
theme_set(theme_estat())

setwd("C:/Users/InfoWay/Desktop/projeto fantasma")
dados<-read_xlsx("tent 1.xlsx", sheet=2)

#1.quantidade de marcas divulgadas em cada um dos anos ate agosto de 2010----


dados <- rename(dados, Idade = Age)
dados <- dados %>%
  mutate(ano = 2010.75 - Idade)
dados <- dados %>%
  mutate(ano = round(2010.75 - Idade))
analise1 <- dados %>% 
  count(ano)

ggplot(analise1, aes(x=ano, y=n)) +
  geom_line(size=1,colour="#A11D21") + 
  geom_point(colour="#A11D21",size=2) +
  labs(x="Anos", y="Quantidade de marcas") +
  theme_bw() + 
  scale_x_continuous(breaks =seq(1800,2010,30)) +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = "black"))



ggsave("analise1.pdf", width = 158, height = 93, units = "mm")



#2.relação ou não entre a idade das marcas e a avalia??o de ousadia----




cor(dados$Idade, dados$Daring_pct, method = "pearson" )
analise2 <- dados %>%
  na.exclude(Idade,`Q7_1 - Daring`)
cor(analise2$Idade, analise2$`Q7_1 - Daring` , method = "spearman")
ggplot(dados, aes(x=Idade, y=`Q7_1 - Daring`)) + 
  geom_point(colour="#A11D21", size=3) +
  labs(x="Idade", y="Avalia??o de ousadia") +
  theme_bw() +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = "black"))



ggsave("analise2.pdf", width = 158, height = 93, units = "mm")



#3.quantas marcas se encaixam em cada uma das categorias----




dados$Category[dados$Category==1]<-"Produtos de beleza"
dados$Category[dados$Category==2]<-"Bebidas"
dados$Category[dados$Category==3]<-"Carros"
dados$Category[dados$Category==4]<-"Produtos infantis"
dados$Category[dados$Category==5]<-"Produtos de vestuario"
dados$Category[dados$Category==6]<-"Lojas de departamento"
dados$Category[dados$Category==7]<-"Serviços financeiros"
dados$Category[dados$Category==8]<-"Comida e jantar"
dados$Category[dados$Category==9]<-"Produtos e serviços de saúde"
dados$Category[dados$Category==10]<-"Projeto e decoração de casa"
dados$Category[dados$Category==11]<-"Produtos para o lar"
dados$Category[dados$Category==12]<-"Midia e entretenimento"
dados$Category[dados$Category==13]<-"Esportes e hobbies"
dados$Category[dados$Category==14]<-"Produtos de tecnologia"
dados$Category[dados$Category==15]<-"Telecomunições"
dados$Category[dados$Category==16]<-"Serviços de viagem"
freq <- table(dados$Category)
porcent <- round(prop.table(freq)*100,2)
porcent1 <- gsub("\\.", ",", porcent)
rotulo <- paste(freq, " (", porcent1,"%",")",sep="")

dados3 <- data.frame(freq,porcent)
dados3$Var1.1 <- NULL


ggplot(data=dados3, aes(x= fct_reorder(Var1, freq, .desc=T), y=Freq)) +
  geom_bar(stat="identity", fill="#A11D21", width = 0.7, na.rm = T)+
  geom_text(aes(label= rotulo), vjust= 0.1, hjust= -0.1, size=3)+
  ylab("Quant. ou porcent. de marcas") +
  xlab("Categorias") +
  coord_flip()+
  ylim(0,150)+
  theme_estat()


ggsave("analise3.pdf", width = 158, height = 93, units = "mm")


#4.relação entre risco em caso de decis?o errada e importancia da decis?o----
dados
cor(dados$`Q4: Risk in case of wrong decision` ,dados$`Q2: importance of decision`)
ggplot(dados, aes(x=dados$`Q4: Risk in case of wrong decision`, y=dados$`Q2: importance of decision`)) + 
  geom_point(colour="#A11D21", size=3) +
  labs(x="Risco em caso de decis?o errada", y="Import?nciada decis?o") +
  theme_bw() +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = "black"))




ggsave("analise4.pdf", width = 158, height = 93, units = "mm")


#5.compara??o entre qualidade da marca e sua visibilidade----
dados$Premium[dados$Premium==1]<-"Premium"
dados$Premium[dados$Premium==2]<-"Middle"
dados$Premium[dados$Premium==3]<-"Mix"
dados$Premium<-as.factor(dados$Premium)
ggplot(dados, aes(x=Premium, y=Visibilidade)) +
  geom_boxplot(fill=c("#A11D21"), width = 0.5) +
  guides(fill=FALSE) +
  stat_summary(fun.y="mean", geom="point", shape=23, size=3, fill="white")+
  labs(x="Qualidade", y="Visibilidade")+
  theme_bw() +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line.y = element_line(colour = "black"))




ggsave("analise5.pdf", width = 158, height = 93, units = "mm")



#6.rela??o entre o tipo de produto e sua qualidade----




dados$Product[dados$Product==1]<-"Produto"
dados$Product[dados$Product==2]<-"Serviço"
dados$Product[dados$Product==3]<-"Mix"
cdata <- table(dados$Product, dados$Premium)
(pdata <- prop.table(cdata))

dados4 <- as.data.frame(pdata)

ggplot(dados4, aes(x = Var1, y = Freq)) +
  geom_col(aes(fill = Var2), position = "dodge") +
  scale_y_continuous(labels = scales::percent) + theme_estat() + labs(x="Produto", y="Porcentagem", fill="qualidade")


  

  
  
ggsave("analise6.pdf", width = 158, height = 93, units = "mm")

